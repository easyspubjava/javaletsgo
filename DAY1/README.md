# 객체, 클래스, 속성, 함수, 메서드

## 객체 (Object)

- 객체 지향 프로그램에서는 주체나 대상이 되는 경우 객체로 정의할 수 있음

- 명사나 동사의 명사형이 객체로 표현되는 경우가 많고

- 역할(role), 책임(responsibility), 협력(collaboration)

- 역할 :특정 협력안에서 수행하는 책임의 집합  추상클래스, 인터페이스

- 책임 : 협력에 참여하기 위해 객체가 수행하는 행동 doing, knowing  추상클래스, 인터페이스를 구현

- 협력 : 객체간의 상호 작용 message 전달을 통해 이루어짐  

## 클래스
- blue print of Object

- 클래스 full name, 자바 clsss가 만들어지는 곳 

## 속성
- Object의 특성으로 프로그램에서는 주로 적절한 자료형을 활용하여 멤버 변수로 선언

- property, attribute member variable

## 함수

- 하나의 기능을 구현한 일련의 코드

- 함수의 stack메모리 구조

## 메서드

- 객체의 기능을 구현한 코드


## 자바 Coding Convention

- 클래스의 시작은 대문자로 시작

- 메서드나 변수의 시작은 소문자로 시작

- 중간에 단어가 바뀌면 대문자로 : Camel notation

- 패키지의 이름은 모두 소문자

- java 파일 하나에 클래스는 여러 개가 있을 수 있지만, public 클래스는 하나이고, public 클래스와 .java 파일의 이름은 동일함


## 마무리

- 프로그램이란?, 속성의 의미, 메서드

- 절차지향 프로그래밍(Procedural programming) vs 객체지향 프로그래밍(Object Oriented Programming) vs 함수형 프로그래밍(Funcitonal Programming)

- 자바 프로젝트 구조, 소스 위치, class파일 위치

- help 보기

- 디버깅

## NEXT...

- 인스턴스, 힙메모리, 참조변수, 참조값, 생성자, 생성자 오버로딩, 정보은닉, 캡슐화
