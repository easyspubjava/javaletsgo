# 참조형 변수, this, static, 객체간 협력, singleton pattern

## 참조형 변수

- 객체간 합성, 연관, 집약 관계 구현

- 객체가 멤버 변수의 타입으로


## this

- this 가 가리키는 값은?

- 메모리의 상태


## static

- static 변수가 필요한 이유?  vs. C언어 global 변수

- 두 개의 인스턴스를 생성하고 메모리 상태?

- 인스턴스 변수 vs. 클래스 변수

- 인스턴스의 생성과 무관하게 클래스의 이름으로 사용

- static 메서드에서 인스턴스 변수를 사용하지 못하는 이유?

- ToDo : 회사원이 입사했을때 사번을 하나씩 생성해주자

     카드 회사에서 카드가 발행 될 때마다 카드 번호를 부여해주자
        
## singleton pattern 

- 같이 코딩해 보는 걸로~

- Car, CarFactory, CarFactoryTest

```
public class CarFactoryTest {

	public static void main(String[] args) {
		CarFactory factory = CarFactory.getInstance();
		Car mySonata = factory.createCar();
		Car yourSonata = factory.createCar();
		
		System.out.println(mySonata.getCarNum());     //10001 출력
		System.out.println(yourSonata.getCarNum());   //10002 출력
	}
}
```

## 객체간 협력

- 같이 코딩해보는 걸로~
```
public class TakeTransTest {

	public static void main(String[] args) {
		Student studentJ = new Student("James", 5000);
		Student studentT = new Student("Tomas", 10000);
		
		Bus bus100 = new Bus(100);
		
		Subway subwayGreen = new Subway(2);
		
		
		studentJ.takeBus(bus100);
		studentT.takeSubway(subwayGreen);
		
		studentJ.showInfo();
		studentT.showInfo();
		
		bus100.showBusInfo();
				
		subwayGreen.showSubwayInfo();
	}

}
```

## NEXT....

- 배열, 객체 배열, 배열의 복사, ArrayList 
