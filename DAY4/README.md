# 배열, 객체 배열, 배열의 복사, ArrayList

## 1. 배열

- 배열을 왜 쓰는가?

- 자바 배열은 new 해서 사용한다

- 배열의 길이

- 배열은 논리적 위치와 물리적 위치가 동일하다

- 배열에서 인덱스 연산이 빠른 이유?

- 배열은 0 부터 시작

## 2. 객체 배열

- 기본 자료형과 객체 배열의 차이?

https://gitlab.com/easyspubjava/javacoursework/-/blob/master/Chapter2/2-21/README.md


## 3. 배열의 복사

- 객체 배열에서 복사 하는 방법

- 깊은 복사 vs. 얕은 복사


## 4. ArrayList

- javaDoc보기


## 과제

https://gitlab.com/easyspubjava/javacoursework/-/blob/master/Chapter2/2-24/README.md


## 5. NEXT...
- 상속, super, 업캐스팅, 메서드 오버라이딩, protected, 가상함수
