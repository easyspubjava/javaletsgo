# 인스턴스, 힙 메모리, 참조 변수, 참조 값, 생성자, 생성자 오버로딩, 정보은닉, 캡슐화


## 인스턴스

- 클래스 기반으로 생성된 객체 new 키워드 사용

- 각각 다른 멤버 변수 메모리를 가지게 됨

- 생성된 인스턴스는 동적 메모리 (heap memory)에 할당 됨

## 힙메모리(heap memory)

// 자바 언어의 특징???
## 참조변수, 참조값

Student studentLee = new Student();  

studentLee의 위치는???
// 해시코드
// 스택 메모리

## 생성자
// 생성자의 역할?

### 디폴트 생성자

//
### 생성자 오버로딩

// 생성자를 오버로딩 하는 이유???
// 디폴트 생성자는 왜 필요할까?
// this 

## infomation hiding


### public, private, protected

//public으로 사용하는 경우와 private으로 두고 get()/set() 을 사용할때의 차이는 무엇인가?

## encapsulation

// public으로 모두 오픈하면 사용자에게 일관된 사용법이 제공되지 않고, 사용자가 필요없는 detail을 알아야 한다.
// 객체의 무결성을 해칠수 있다.

## Next...
- 참조형 변수, this, static, 객체간 협력
